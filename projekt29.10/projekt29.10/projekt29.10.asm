/*
 *   Authors: Marcin Cho�oniewski & Stanis�aw Szufa
 */ 

 .include "m32def.inc"

.DSEG                    ; segment danych
.ORG 0x0060                ; zaczyna zapis od adresu 0x0060 - tak jak domyslnie
.EQU LEN = 257                ; d�ugo�� liczby w bajtach
tab_1: .BYTE LEN            ; tablica LEN-bitowa
tab_2: .BYTE LEN        ; te� ^

.CSEG                    ; segment kodu
.ORG 0                    ; zaczyna od adresu 0



LDI XL, low(tab_1)    ; do XL zapisuje mniejsze 8 bitow
LDI XH, high(tab_1) ; do XH zapisuje wieksze 8 bitow
LDI YL, low(tab_2)    ; do YL zapisuje mniejsze 8 bitow
LDI YH, high(tab_2) ; do YH zapisuje wieksze 8 bitow 
LDI ZL, low(LEN) ;	;analogicznie z mniejszymi/wi�kszymi bitami d�ugo�ci tablic
LDI ZH, high(LEN) ;

LDI R20, low(RAMEND)    ; przygotowanie stosu
OUT SPL, R20
LDI R20, high(RAMEND)
OUT SPH, R20

CLC   ; czyszczenie flagi 'carry'



CALL DODAJ	; wywo�anie procedury  DODAJ operuj�cej na 2 liczbach (w X i Y) i ich d�ugo�ci (w Z)


; W X jest pierwsza liczb
; W Y jest druga liczba
; W Z jest dlugosc w bajtach



DODAJ:
    PUSH R16	; zachowanie zawarto�ci rejestr�w R16 i R17 poprzez wrzucenie ich na stos
    PUSH R17
	PUSH R18	; flaga dla pierwszej operacji -> ADC
	PUSH R19	; flaga dla drugiej operacji -> SBIW
	PUSH R20

	LDI R18, 0
	LDI R19, 0
	LDI R20, 0

    LOOP:
		; dodatkowe linijki kodu (w por�wnaniu z poprzednim commitem) zwi�zane s� z obs�ugiem flagi carry przy 2 operacjach - nowe zaznaczone akapitami
			; zaladowanie "wartosci flagi" C z rejestru R18
			CLC
			CPSE R18, R20
			SEC	;ustawia tylko w przypadku gdy R18 != 0

        LD R16, X+	; w ka�dej kolejnym przebiegu p�tli przyporz�dkowywanie coraz bardziej znacz�cych bajt�w z tablicy 1 do rejestru R16
        LD R17, Y	; i z tablicy 2 do R17
        ADC R16, R17	; dodawanie bajt�w wskazywanych przez rejestry (z flag�)
        ST Y+, R16		; wrzucenie rezultatu powy�szego dodawania do kom�rki na kt�r� wskazuje adres z rejestru Y, nast�pnie zwi�kszanie tego adresu o 1
        
			; "zapisanie warto�ci flagi" do rejestru R18
			LDI R18, 1
			BRCS endA
			LDI R18, 0	; gdy C=0
			endA:

			; zaladowanie "wartosci flagi" C z rejestru R19
			CLC
			CPSE R19, R20
			SEC	;ustawia tylko w przypadku gdy R19 != 0

        SBIW Z, 1		; odejmujemy 1 od liczby kt�ra informuje nas, ile jeszcze bajt�w musimy doda� (od d�. LEN do 0)
		BREQ FINISH		; gdy warto�� wskazywana przez Z osi�gnie 0, ko�czymy p�tl�
		
			; "zapisanie warto�ci flagi" do rejestru R19
			LDI R19, 1
			BRCS endB
			LDI R19, 0 ;gdy C=0
			endB:

        JMP LOOP		; w przeciwnym razie kolejny obieg p�tli

    FINISH:
		POP R20
		POP R19
		POP R18
        POP R17			; na koniec przywracamy pocz�tkowe stany rejestr�w R16 i R17 przez wyci�gni�cie warto�ci ze stosu
        POP R16   
    ret