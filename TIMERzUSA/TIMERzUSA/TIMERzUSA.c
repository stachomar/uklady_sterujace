/*
 * TIMERzUSA.c
 *
 * Created: 2015-01-07 12:12:02
 *  Authors: Marcin Cho�oniewski i Stanis�aw Szufa
 */
#include "cooperative_scheduler.h"
#include "HD44780.h"
#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include <math.h>
#include <stdio.h>
#include <avr/interrupt.h>

#define col1 14
#define col2 13
#define col3 11
#define col4 7

#define num0 192
#define num1 249
#define num2 164
#define num3 176
#define num4 153
#define num5 146
#define num6 130
#define num7 248
#define num8 128
#define num9 144

#define empty 255

uint8_t values[4] = {192,192,192,192};
uint8_t col[] = {col1,col2,col3,col4};
uint8_t num[] = {num0,num1,num2,num3,num4,num5,num6,num7,num8,num9};

uint8_t Column = 0;
int sec = 0;
int min = 0;
int hour = 0;
int displayer_mode = 1; // 1 = MMSS , 0 = HHMM
int LCD_mode = 0;     // 0 neutral, 1 set new time, 2 set new alarm, 3 view alarms
int current_key = 0;
int uart_mode = 0;
int licznik = 0;
int active_digit = -1; // 0123 = HHMM
int new_time[4] = {0,0,0,0}; // H H M M
int alarm[16] = {-1,0,0,0, -1,0,0,0, -1,0,0,0, -1,0,0,0,}; // po 4 pola na alarm - w sumie 4 alarmy
int slot = -1;
int view = -1;
int last_digit = -1;
char* last_line_A = "";
char* last_line_B = "";
uint8_t print_now = 0;
char words[256];
int ctr = 0;

void LCD_print_line_A(char* arg){
	// uaktualnienie pierwszej lini na LCD bez zmiany drugiej
    last_line_A = arg;
    LCD_Clear();
    LCD_GoTo(0,0);
    LCD_WriteText(arg);
    LCD_GoTo(0,1);
    LCD_WriteText(last_line_B);
}

void LCD_print_line_B(char* arg){
	// uaktualnienie drugiej lini na LCD bez zmiany pierwszej
    last_line_B = arg;
    LCD_Clear();
    LCD_GoTo(0,0);
    LCD_WriteText(last_line_A);
    LCD_GoTo(0,1);
    LCD_WriteText(arg);
}

void change_LCD_mode(int mode){
	// zmiana podstawowego trybu dzia�ania
    if(mode == 0){
        LCD_mode = 0;
    }else if(LCD_mode == 0 || LCD_mode == 3){
        LCD_mode = mode;
    }
}

void init_usart(uint16_t baud_rate) {
    UBRRH =  (baud_rate >> 8);
    UBRRL =   baud_rate;
    UCSRB |= (1<<RXEN) | (1<<TXEN) | (1<<RXCIE);  
    UCSRC |= (1<<URSEL) | (1<<USBS) | (3<<UCSZ0);  
}

char cmd_read(){
    if(UCSRA & (1<<RXC))
        return UDR;
    return 255;
}

ISR(USART_RXC_vect){
    return 0;
}

void displayer(void){    
	// wy�wietlanie godziny na 7-segmentowca
    licznik++;
    if(licznik%2 == 0){
        PORTA = values[Column];
        PORTB = col[Column];   
        Column++;
        Column %= 4;
    }   

}

void MSchanger(){
	// ustawienie godziny dla trybu MM:SS
    values[0] = num[sec%10];
    values[1] = num[sec/10];
    values[2] = num[min%10] - 128; // with a dot
    values[3] = num[min/10];
}

void HMchanger(){
	// ustawienie godziny dla trybu HH:MM
    values[0] = num[min%10];
    values[1] = num[min/10];
    values[2] = num[hour%10] - 128; // with a dot
    values[3] = num[hour/10];
}



void timer(void){
	// aktualizacja czasu co sekund� + sprawdzanie alarm�w co minut�
    sec++;
    if(sec == 60){
        sec = 0;
        min++;       
        if(min == 60){
            min = 0;
            hour++;
            if(hour == 24){
                hour = 0;           
            }
        }                   
        if(displayer_mode==1)
            MSchanger();
        else
            HMchanger();                   
        for(int i=0; i<4; ++i){
            if(alarm[i*4] != -1){
                int hh = alarm[i+0]*10 + alarm[i+1];
                int mm = alarm[i+2]*10 + alarm[i+3];
                if(hh == hour && mm == min){
                    LCD_print_line_A("ALARM!");
                    char to_print[5];
                    sprintf(to_print, "%d%d:%d%d", alarm[i*4],alarm[i*4+1],alarm[i*4+2],alarm[i*4+3]);
                    LCD_print_line_B(to_print);
                }
            }
        }
    }
    else {       
        if(displayer_mode==1)
        MSchanger();
        else
        HMchanger();
    }
}


void clear_new_time(){
	// czyszczenie tablicy new_time
    new_time[0] = 0;
    new_time[1] = 0;
    new_time[2] = 0;
    new_time[3] = 0;
}

void change_display_mode(){
	// zmiana trybu wy�wietlacza pomi�dzy MM:SS i HH:MM
    if(displayer_mode==1)
        displayer_mode = 0;
    else
        displayer_mode = 1;
}


void set_new_time(){
	// ustawienie nowego czasu
    hour = new_time[0]*10 + new_time[1];
    min = new_time[2]*10 + new_time[3];
    sec = 0;
   
    displayer_mode = 0;
    clear_new_time();
}

void set_new_alarm(){
	// ustawienie nowego alarmu
    for(int i=0;i<4;++i){
        alarm[slot*4 + i] = new_time[i];
    }
    clear_new_time();
   
    slot = -1;
}

void set_time_error(){
    LCD_print_line_A("Try again: ");   
    LCD_print_line_B("--:--");
	active_digit = 0;
    clear_new_time();
}

void check_new_time(){
	// sprawdzenie czy wprowadzony czas jest poprawny
    if(new_time[0] > 2) set_time_error();
    else if(new_time[0]==2 && new_time[1] > 3) set_time_error();
    else if(new_time[2] > 5) set_time_error();
    else{
        active_digit = -1;
        if(LCD_mode == 1){                       
            set_new_time();
            LCD_print_line_A("Time set.");   
        }           
        else if(LCD_mode == 2){
            set_new_alarm();           
            LCD_print_line_A("Alarm set.");
        }               
        change_LCD_mode(0);
        }       
}

void numbers(int current_key){
	// wprowadzanie godziny
    if(active_digit > -1){
        new_time[active_digit] = current_key;
        char to_print[5];
       
        if(active_digit==0)
            sprintf(to_print, "%d-:--", new_time[0],new_time[1],new_time[2],new_time[3]);           
        else if(active_digit==1)
            sprintf(to_print, "%d%d:--", new_time[0],new_time[1],new_time[2],new_time[3]);
        else if(active_digit==2)
            sprintf(to_print, "%d%d:%d-", new_time[0],new_time[1],new_time[2],new_time[3]);
        else if(active_digit==3)
            sprintf(to_print, "%d%d:%d%d", new_time[0],new_time[1],new_time[2],new_time[3]);
           
        LCD_print_line_B(to_print);
        active_digit++;
        if(active_digit > 3){
            check_new_time();
        }
    }   
}

void change_time(){   
	// zmiana trybu
	// na tryb ustawiania czasu
    change_LCD_mode(1);
    if(LCD_mode == 1){
        active_digit = 0;
        if(uart_mode)
            LCD_print_line_A("Set the time: u");   
        else
            LCD_print_line_A("Set the time: k");   
        LCD_print_line_B("--:--");
    }
}

void set_alarm(){   
	// sprawdzenie czy jest wolny slot na alarm, je�eli tak to zmiana trybu
	// na tryb ustawiania alarmu
    if(alarm[0] == -1) slot = 0;
    else if(alarm[4] == -1) slot = 1;
    else if(alarm[8] == -1) slot = 2;
    else if(alarm[12] == -1) slot = 3;
    else{
        LCD_print_line_A("No free slots");
        LCD_print_line_B("for new alarm");
    }
   
    if(slot > -1){
        change_LCD_mode(2);
        if(LCD_mode == 2){
            active_digit = 0;
            if(uart_mode)
                LCD_print_line_A("Set the alarm: u");
            else
                LCD_print_line_A("Set the alarm: k");
            LCD_print_line_B("--:--");
        }       
    }   
}

void view_alarms(){
   // przegl�danie alarm�w
    change_LCD_mode(3);
    if(LCD_mode == 3){
        view++;
        char alarm_number[16];
        sprintf(alarm_number, "Alarm number %d", view+1);
        LCD_print_line_A(alarm_number);
        char to_print[5];
        if(alarm[view*4] == -1)
        LCD_print_line_B("empty slot");
        else{
            sprintf(to_print, "%d%d:%d%d", alarm[view*4],alarm[view*4+1],alarm[view*4+2],alarm[view*4+3]);
            LCD_print_line_B(to_print);
        }
        if(view == 3){
            view = -1;
        }
    }   
}

void delete_alarm(){ 
	// usuwa aktualnie przegl�dany alarm
    if(view > -1 && LCD_mode == 3){
        alarm[view*4 + 0] = -1;
        alarm[view*4 + 1] = 0;
        alarm[view*4 + 2] = 0;
        alarm[view*4 + 3] = 0;
    }
    char alarm_number[16];
    sprintf(alarm_number, "Alarm %d deleted", view+1);
    LCD_print_line_A(alarm_number);
    LCD_print_line_B("Slot is empty");
}



void change_uart_mode(){
	// zmiany sposobu wprowadzania liczb pomi�dzy uartem i przyciskami
    if(LCD_mode==0 || LCD_mode==3){
        if(uart_mode == 0){
            uart_mode = 1;
            LCD_print_line_A("MODE CHANGED TO:");
            LCD_print_line_B("UART");
        }
        else {
            uart_mode=0;
            LCD_print_line_A("MODE CHANGED TO:");
            LCD_print_line_B("KEYBOARD");
        }   
    }       
}

void cmd_write(char data){
	// przetowrzenie znaku z uarta na nasz system
    int int_data = (int)(data-48);
    if(int_data < 10 && int_data >= 0)
        numbers(int_data);
}

void uart_handler(){
    if(uart_mode == 1){
        char data;
        data = cmd_read();
        if(data != 255) cmd_write(data);
    }   
}

void operate(int current_key){
	// wyznacznie dzia�a, zale�nie od naci�nietego przycisku
    if(current_key==15)
        change_display_mode();
    else if(current_key==14)
        change_time();
    else if(current_key==13)
        set_alarm();
    else if(current_key==12)   
        change_uart_mode();
    else if(current_key==11)
        view_alarms();
    else if(current_key==10)       
        delete_alarm(); // only if view_mode active
    else if(current_key>=0)
        if(uart_mode == 0)
            numbers(current_key);
}

void keyboard_handler(void){
	// odczytanie kt�ry przycisk zosta� wci�ni�ty
    int key1=0;
    int key2=0;
   
    PORTC = 0x00;
    DDRC = 0x0f;
    PORTC = 0xf0;
    for(int i=0;i<10;++i);
    key1 = PINC;
    key1 = key1 >> 4;
    key1 = (~key1) & 0x0f;
    PORTC = 0x00;
    DDRC = 0xf0;
    PORTC = 0x0f;
    for(int i=0;i<10;++i);
    key2 = PINC;
    key2 = (~key2) & 0x0f;
   
    key1 = log(key1) / log(2) +1; //oblicza logarytm o podst. 2 z liczby w key1 (1,2,4 lub 8)
    key2 = log(key2) / log(2);
   
    int current_key =  key1+key2*4 - 1; //number from [0,15]
   
    if(current_key != last_digit){       
        operate(current_key);
        last_digit = current_key;
    }
}

int main ()
{
    init_timer();
    init_usart(25);
   
    DDRA = 0xFF;
    PORTA = 192;
    DDRB = 0xFF;
    PORTB = 0;
    DDRC = 0xFF;
    PORTC = 0;
    LCD_Initalize();    //port D
    add_task(0, 1000, timer, NULL); //obs�uga przerwania!
    add_task(1, 50, keyboard_handler, NULL); //obs�uga klawiatury
    add_task(2, 200, uart_handler, NULL);
    execute();
  
    return 0;
}

