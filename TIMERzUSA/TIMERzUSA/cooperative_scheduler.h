#ifndef _COOPERATIVE_SCHEDULER_H_
#define _COOPERATIVE_SCHEDULER_H_

typedef void (*func_ptr)();
typedef void (*func_params)(void *);


typedef struct Task{
	unsigned int interval;
	unsigned int ready_in;
	func_ptr func;
	func_params params;
};

void add_task(int priority, int period, func_ptr func, func_params params);

void schedule();

void execute();

#endif