#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include "cooperative_scheduler.h"

#define MAX_NUMBER_OF_TASKS 10

struct Task task_array[MAX_NUMBER_OF_TASKS];

ISR(TIMER0_COMP_vect) {
	displayer();
	schedule();
}


void init_timer() {
	OCR0 = 250;
	TIMSK |= 1 << OCIE0;
	TCCR0 |= 1 << WGM01 | 1 << CS00 | 1 << CS01;
	sei();
}

void schedule() {
	int i;
	for(i = 0; i < MAX_NUMBER_OF_TASKS; ++i) {
		if (task_array[i].ready_in != 0)
		task_array[i].ready_in--;
	}
}

void add_task(int priority, int period, func_ptr func, func_params params) {
	struct Task * task = task_array + priority;
	task->func = func;
	task->interval = period;
	task->ready_in = period;
	task->params = params;
}

void execute() {
	init_timer();
	struct Task* it;
	while (1) {
		cli();
		for(it = task_array; it < task_array + MAX_NUMBER_OF_TASKS; ++it) {
			if (it->func != NULL && it->ready_in == 0) {
				it->ready_in = it->interval;
				sei();
				it->func(it->params);
				break;
			}
		}
		sei();
	}
}