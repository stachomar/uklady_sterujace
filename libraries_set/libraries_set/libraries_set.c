/*
* Author: Stasiu Szufa & Marcin Chołoniewski
*/
#define F_CPU 16000000UL

#include "cooperative_scheduler.h"
#include "keyboard.c"
#include "7-segment.c"
#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stddef.h>
#include <math.h>

ISR(TIMER_COMP_vect)
{
	schedule();
}


int main(void) {
	init_timer();
	
	DDRA = 0xFF;
	PORTA = 192;
	DDRB = 0xFF;
	PORTB = 0;
	schedule();
	addTask(0, 1, displayer, NULL);
	addTask(1, 8, keyboard_handler, NULL); //obsługa przerwania!
	execute();
}