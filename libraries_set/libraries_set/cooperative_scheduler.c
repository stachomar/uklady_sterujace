#include "cooperative_scheduler.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

struct Task MyTasks[MAX_NUMBER_OF_TASKS];

void init_timer(void) {
	OCR0 = 250;
	TIMSK |= 1 << OCIE0;
	TCCR0 |= 1 << WGM01 | 1 << CS00 | 1 << CS01;
	sei();
}

void addTask(int priority, int period, task_ptr func_ptr, task_params params){
	struct Task * new_task = MyTasks + priority;

	new_task->period = period;
	new_task->time_to_wait = period;
	new_task->params = params;
	new_task->my_func = func_ptr;
	new_task->ready = 0;
	new_task->created = 1;
}
/*
void clear_task_table(){
	for(int i=0; i<MAX_NUMBER_OF_TASKS; ++i){
		MyTasks[i].created = 0;
	}
}
*/
void execute(){
	//clear_task_table();
	init_timer();
	while(1){
		cli();
		for(struct Task* ptr = MyTasks, ptr < MyTasks+MAX_NUMBER_OF_TASKS; ++ptr){
			if(ptr->created != 0){
				if(ptr->ready == 1){
					ptr->my_func(ptr->params);
					ptr->ready = 0;
					break;
				}
			}
		}
		sei();
	}
}

void schedule()
{
	for(int i=0; i<MAX_NUMBER_OF_TASKS; i++){
		if(MyTasks[i].time_to_wait == 0){
			cli();
			MyTasks[i].ready = 1;
			MyTasks[i].time_to_wait = MyTasks[i].period;
			sei();
		}
		else {
			MyTasks[i].time_to_wait--;
		}
	}
}