/*
 * _7_segment.c
 *
 * Created: 2014-11-26 19:06:30
 *  Authors: Marcin Chołoniewski & Stanisław Szufa
 */

#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include <math.h>

#define col1 14
#define col2 13
#define col3 11
#define col4 7

#define num0 192
#define num1 249
#define num2 164
#define num3 176
#define num4 153
#define num5 146
#define num6 130
#define num7 248
#define num8 128
#define num9 144

#define empty 255

uint8_t values[4] = {192,192,192,192};
uint8_t col[] = {col1,col2,col3,col4};
uint8_t num[] = {num0,num1,num2,num3,num4,num5,num6,num7,num8,num9};

uint8_t Column = 0;

void displayer(void){
	PORTA = values[Column];
	PORTB = col[Column];
	_delay_ms(2);        //bug z miganiem
	
	Column++;
	Column %= 4;

}

void changer(int val){
	values[0] = num[val%10];
	val /=10;
	values[1] = num[val%10];
	val /=10;
	values[2] = num[val%10];
	val /=10;
	values[3] = num[val];
}

void changer_dot(int val){
	values[0] = num[val%10];
	val /=10;
	values[1] = num[val%10] - 128;
	val /=10;
	values[2] = num[val%10];
	val /=10;
	values[3] = num[val];
}

int T = 0;

void timer(void){
	if(T < 10000)
	changer_dot(T);
	else
	changer(T/10);
	T++;
}