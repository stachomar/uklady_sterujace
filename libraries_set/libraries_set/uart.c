#include <avr/io.h>
#include <avr/interrupt.h>

uint8_t print_now = 0;
char words[256];
int ctr = 0;

void init_usart(uint16_t baud_rate) {
    UBRRH =  (baud_rate >> 8);
    UBRRL =   baud_rate;   
    UCSRB |= (1<<RXEN) | (1<<TXEN) | (1<<RXCIE);   
    UCSRC |= (1<<URSEL) | (1<<USBS) | (3<<UCSZ0);
   
}

char read(){
    while(!(UCSRA & (1<<RXC)));
    return UDR;
}

void write(char data){
    while(!(UCSRA & (1<<UDRE)));
    UDR = data;
}


ISR(USART_RXC_vect){
    char data = read();       
    if(data==13){
        print_now = 1;
    }else{       
        words[ctr++] = data;
    }
}

int main(void)
{
    char data;   
    init_usart(25);   
   
    sei();
    while(1){       
       
        //data = read();
        //write(data);
       
       
        if(print_now == 1){
            for(int i=0;i<ctr;++i){
                write(words[i]);       
            }           
            ctr = 0;
            print_now = 0;
        }
    }
    cli();
}