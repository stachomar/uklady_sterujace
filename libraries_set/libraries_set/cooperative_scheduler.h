#ifndef _COOPERATIVE_SCHEDULER_H
#define _COOPERATIVE_SCHEDULER_H

#define MAX_NUMBER_OF_TASKS 16

typedef void (*task_ptr)(void *);
typedef void (*task_params)(void *);

typedef struct Task{
	int ready;
	task_ptr my_func; // ptr to function
	int period; // in ms, stable
	int time_to_wait; // in ms, changing
	task_params params;
	int created;
};

void init_timer();

void schedule();

//void clear_task_table();

void execute();

void addTask(int,int, task_ptr, task_params);

#endif _COOPERATIVE_SCHEDULER_H