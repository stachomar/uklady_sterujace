#define F_CPU 16000000UL

#include "7-segment.c"
#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include <math.h>

void keyboard_handler(void){
	int key1=0;
	int key2=0;
	
	PORTC = 0x00;
	DDRC = 0x0f;
	PORTC = 0xf0;
	_delay_ms(4);
	key1 = PINC;
	key1 = key1 >> 4;
	key1 = (~key1) & 0x0f;
	PORTC = 0x00;
	DDRC = 0xf0;
	PORTC = 0x0f;
	_delay_ms(4);
	key2 = PINC;
	key2 = (~key2) & 0x0f;
	
	key1 = log(key1) / log(2) +1; //oblicza logarytm o podst. 2 z liczby w key1 (1,2,4 lub 8)
	key2 = log(key2) / log(2);
	changer(key1+key2*4);
}