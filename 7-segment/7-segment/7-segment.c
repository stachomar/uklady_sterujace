/*
 * _7_segment.c
 *
 * Created: 2014-11-26 19:06:30
 *  Authors: Marcin Chołoniewski & Stanisław Szufa
 */

#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>

#define col1 14
#define col2 13
#define col3 11
#define col4 7

#define num0 192
#define num1 249
#define num2 164
#define num3 176
#define num4 153
#define num5 146
#define num6 130
#define num7 248
#define num8 128
#define num9 144


#define empty 255



int main(void)
{

    DDRA = 0xFF;
    PORTA = 0;
    DDRB = 0xFF;
    PORTB = 0;
    //uint8_t licznik=0;
   
    uint8_t col[] = {col1,col2,col3,col4};
    uint8_t num[] = {num0,num1,num2,num3,num4,num5,num6,num7,num8,num9};
   
    int sek[] ={0,0,0,0};
    int time = 0;
       
    while(1)
    {
        if(time < 1000){
            for(int i=0;i<20;i++){   
                if(i%4 == 1){
                    PORTA = num[sek[i%4]]-128;
                } else {               
                    PORTA = num[sek[i%4]];
                }   
                PORTB = col[i%4];           
                _delay_ms(5);               
            }       
        } else {
            for(int i=0;i<200;i++){   
                PORTA = num[sek[i%4]];
                PORTB = col[i%4];
                _delay_ms(5);
            }
        }       
        time++;
        sek[0] = time % 10;
        sek[1] = (time/10) % 10;
        sek[2] = (time/100) % 10;
        sek[3] = (time/1000) % 10;
       
    }
}