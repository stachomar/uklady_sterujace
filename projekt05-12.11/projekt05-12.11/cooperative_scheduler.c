#include "cooperative_scheduler.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stddef.h>

void addTask(int priority, int period, task_ptr func_ptr, void * params){
	Task new_task;

	new_task.period = period;
	new_task.time_to_wait = period;
	new_task.params = params;
	new_task.my_func = func_ptr;
	new_task.ready = 0;
	new_task.created = 1;

	MyTasks[priority] = new_task;
}

void clear_task_table(){
	for(int i=0; i<MAX_NUMBER_OF_TASKS; ++i){
		MyTasks[i].created = 0;
	}
}

void execute(){
	clear_task_table();
	while(1){
		for(int i=0; i<MAX_NUMBER_OF_TASKS; i++){
			if(MyTasks[i].created != 0){
				if(MyTasks[i].ready == 1){
					cli();
					MyTasks[i].my_func(MyTasks[i].params);
					MyTasks[i].ready = 0;
					break;
					sei();
				}
			}
		}
	}
}

void schedule()
{
	for(int i=0; i<MAX_NUMBER_OF_TASKS; i++){
		if(MyTasks[i].time_to_wait == 0){
			cli();
			MyTasks[i].ready = 1;
			MyTasks[i].time_to_wait = MyTasks[i].period;
			sei();
		}
		else {
			MyTasks[i].time_to_wait--;
		}
	}
}