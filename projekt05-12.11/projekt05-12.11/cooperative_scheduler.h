#ifndef _COOPERATIVE_SCHEDULER_H
#define _COOPERATIVE_SCHEDULER_H

#define MAX_NUMBER_OF_TASKS 16

typedef void (*task_ptr)(void *);

typedef struct{
	int ready;
	task_ptr my_func; // ptr to function
	int period; // in ms, stable
	int time_to_wait; // in ms, changing
	void * params;
	int created;
}Task;

static Task MyTasks[MAX_NUMBER_OF_TASKS];

void schedule();

void clear_task_table();

void execute();

void addTask(int,int, void (*func_ptr)(), void*);

#endif