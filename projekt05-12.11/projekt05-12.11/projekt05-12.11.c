/*
* Author: Stasiu Szufa & Marcin Chołoniewski
*/
#include "cooperative_scheduler.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stddef.h>

ISR(TIMER_COMP_vect)
{
	schedule();
}


void keyboardTask(void * params){
}

void serialReceiveTask(void * params){
}

void watchDogTask(void * params){
}



int main(void) {
	sei(); // set global interrupt enable
	OCR0 = 250;
	TCCR0 |= (1 << WGM01); // set CTC mode
	TIMSK |= (1 << OCIE0); // set single timer
	TCCR0 |= (1 << CS01)|(1 << CS00); // set prescaler to 64

	schedule();
	addTask(0,50, keyboardTask, NULL);
	addTask(1, 20, serialReceiveTask, NULL);
	addTask(7, 500, watchDogTask, NULL);
	execute();
}