#Przemys�aw Onak

#num1 i num2 to liczby dodawane, num3 to wynik
#program oblicza poprawny wynik i por�wnuje z num3
#liczby big-endian mo�na kopiowa� bezpo�rednio z symulatora


num1 = 'da fd ea da ca af fd 20 39 ea 09 f2 0d af ad fa d0 23 9d 0f 90 92 11 10'.upper()
num2 = 'df af db 4a 3f d4 3d fd f5 df ef 34 fd 43 af df 43 40 23 49 02 34 00 00'.upper()
num3 = 'b9 ad c6 25 0a 84 3b 1e 2f ca f9 26 0b f3 5c da 14 64 c0 58 92 c6 11 10'.upper()

def hexify(val):
    return ''.join('%02x'%i for i in [val])

set1 = num1.split(' ')
set2 = num2.split(' ')
set3 = num3.split(' ')

overflow = 0
errors = 0

for i in range(0, len(set1)):
    expected = (int(set1[i], 16) + int(set2[i], 16))
    if overflow == 1:
        expected += 1
        overflow = 0
    if expected >= 256:
        expected = expected % 256
        overflow += 1
    
    first_part = str(int(set1[i], 16)).zfill(3) + " + " + str(int(set2[i], 16)).zfill(3) + " = " + str(int(set3[i], 16)).zfill(3) + " expected: " + str(expected).zfill(3)
    first_part += " carry " + str(overflow) + " "
    second_part = "\t " + set1[i].zfill(2) + " + " + set2[i].zfill(2) + " = " + set3[i].zfill(2) + " exp: " + hexify(expected).upper().zfill(2)

    if set3[i].zfill(2) != hexify(expected).upper().zfill(2) :
        errors = errors + 1
        second_part += " ERROR!!!"
    else:
        second_part += " OK"

    print(first_part + second_part)


print("Errors: " + str(errors) + "\\" + str(len(set1)))

#tak, wiem, �e to mog�oby dzia�a� szybciej :>