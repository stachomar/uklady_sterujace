.include "m32def.inc"

.DSEG                    ; segment danych
.ORG 0x0060                ; zaczyna zapis od adresu 0x0060 - tak jak domyslnie
.EQU LEN = 2		; d�ugo�� liczby w bajtach
tab_1: .BYTE LEN            ; tablica LEN-bitowa
tab_2: .BYTE LEN        ; te� ^

.CSEG                    ; segment kodu
.ORG 0                    ; zaczyna od adresu 0



LDI R16, 0	;wyczyszczenie rejestr�w do pracy(opcjonalne)
LDI R17, 0

LDI XL, low(tab_1)    ; do XL zapisuje mniejsze 8 bitow
LDI XH, high(tab_1) ; do XH zapisuje wieksze 8 bitow
LDI YL, low(tab_2)    ; do YL zapisuje mniejsze 8 bitow
LDI YH, high(tab_2) ; do YH zapisuje wieksze 8 bitow   
LDI ZL, low(LEN)	; d�ugo�� te� mo�e by� >256 wi�c musi by� zapisana w rejestrze Z
LDI ZH, high(LEN)	; analogicznie do procedury wy�ej

CLC			; czyszczenie flagi 'carry'

Start:
	LD R16, X+
	LD R17, Y
	ADC R16, R17	; add with carry
	ST Y+, R16
	
	SBIW Z, 1
	LDI R17, 0
	
	BREQ FINISH
    JMP LOOP
	/*
*CPSE R20, R18			  ; gdy licznik dojdzie do LEN, to ominie skok i p�jdzie dalej
*JMP Start                ; idzie do start
*
*
*BRCC finish				; sprawdzam czy nie zostala mi jedynka do zapisania, jezeli nie to ko�cz�
*LDI R16, 1
*ST Y, R16
*/
finish:
	JMP finish
