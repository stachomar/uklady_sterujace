#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include <math.h>

#define col1 14
#define col2 13
#define col3 11
#define col4 7

#define num0 192
#define num1 249
#define num2 164
#define num3 176
#define num4 153
#define num5 146
#define num6 130
#define num7 248
#define num8 128
#define num9 144

#define empty 255


uint8_t col[] = {col1,col2,col3,col4};
uint8_t num[] = {num0,num1,num2,num3,num4,num5,num6,num7,num8,num9};

void show(int key){
    PORTB = col[0];
    int key1 = key%10;
    int key2 = (key/10)%10;
    int output[] = {key1, key2};
    for(int i=0;i<200;i++){
             PORTA = num[output[i%2]];
             PORTB = col[i%2];
             _delay_ms(5);
    }
}


int main(void)
{
    while(1){       
        int key1=0;
        int key2=0;
        DDRA = 0xFF;
        PORTA = 0;
        DDRB = 0xFF;
        PORTB = 0;
        show(5);
     
        while(1){
            PORTC = 0x00;
            DDRC = 0x0f;
            PORTC = 0xf0;
            _delay_ms(5);
            key1 = PINC;
            key1 = key1 >> 4;
            key1 = (~key1) & 0x0f;               
            PORTC = 0x00;
            DDRC = 0xf0;
            PORTC = 0x0f;
            _delay_ms(5);
            key2 = PINC;   
            key2 = (~key2) & 0x0f;
           
            key1 = log(key1) / log(2) +1; //oblicza logarytm o podst. 2 z liczby w key1 (1,2,4 lub 8)
            key2 = log(key2) / log(2);
            show(key1 + key2*4);
        }       
           
    }
}